class CreateFeedEntries < ActiveRecord::Migration[5.1]
  def change
    create_table :feed_entries do |t|
      t.string :title
      t.datetime :published_at
      t.text :content
      t.string :url
      t.string :author
      t.integer :feed_id
      t.string :guid

      t.timestamps
    end
  end
end
