namespace :update_feeds do

  # task to sync all the feeds
  task sync: [:environment] do
    Feed.all.each do |feed|
      feed.sync
      puts "#{feed.title} synced"
    end
  end
end
