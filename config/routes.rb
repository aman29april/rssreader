Rails.application.routes.draw do

  root 'feeds#index'

  resources :feeds do
    collection do
      get "latest"
    end
    member do
     resources :feed_entries, only: [:index, :show]

    end
  end
end
