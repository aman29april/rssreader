class FeedEntry < ApplicationRecord

  belongs_to :feed

  validates :title, :published_at, :url, presence: true

  
end
