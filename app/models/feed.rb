class Feed < ApplicationRecord

  has_many :feed_entries, dependent: :destroy

  before_save :set_feed_title
  after_save :sync

  validates :url, uniqueness: true, presence: true, :format => URI::regexp(%w(http https))


 # fetch feed from url and update in database
  def sync
    @feed ||= RSS::Feed.fetch(url)
    add_entries(@feed.items)
  end

  private

  def set_feed_title
    @feed = RSS::Feed.fetch(url)
    self.title = @feed.channel.title
  end


  def add_entries(entries)
    entries.each do |entry|
      local_entry = self.feed_entries.where(title: entry.title).first_or_initialize
      local_entry.update_attributes!( url: entry.link, published_at: entry.published_date, guid: entry.guid)
    end
  end
end
