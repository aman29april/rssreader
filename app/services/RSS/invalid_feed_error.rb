module RSS
  class InvalidFeedError < StandardError
    def message
      "Invalid Feed url"
    end
  end
end
