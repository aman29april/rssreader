module RSS
  class Feed
    class Entry

      attr_accessor :title, :link, :published_at, :guid

      @@published_at_tags = [:pubDate, :published, :updated, :expirationDate, :modified, :'dc:date'].freeze

      def valid?
        title.present? &&  link.present? && published_date.present?
      end

      def tag_to_var_name(tag)
          "@#{tag.to_s.gsub(':','@').intern}"
      end

      def published_date
        @@published_at_tags.each do |tag|
          if instance_variable_get( tag_to_var_name tag)
            return instance_variable_get( tag_to_var_name tag)
          end
        end
      end
    end
  end
end
