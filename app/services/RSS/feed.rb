require 'open-uri'

module RSS
  class Feed
    attr_reader :channel, :items

    def initialize()
      @items = []
      @channel = RSS::Feed::Channel.new
    end

    @@feed_tags = [
      :id,
      :title
    ].freeze

    @@item_tags = [
  		:id,
  		:title,
      :link,
  		:pubDate, :published, :updated, :modified,
      :guid
	  ].freeze


    @@published_at_tags = [:pubDate, :published, :updated, :modified].freeze


    class << self
      # return feed title

      def feed_tags
        @@feed_tags
      end

      def item_tags
        @@item_tags
      end

      # return title of chanel
      def get_title(url)
        feed = parser_class.parse(url)
        feed.channel.title
      end

      # return feed items
      def fetch(url)
        open(url) do |rss|
          parser_class.parse(rss)
        end
      end


      # class  used to parse
      def parser_class
        RSS::Parser
      end

    end


  end
end
