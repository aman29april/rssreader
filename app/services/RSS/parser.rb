require 'open-uri'
require 'cgi'

module RSS
  class Parser
    attr_reader :feed
    def initialize(url)
      @source = open(url).read
      @feed = RSS::Feed.new
      parse_feed
    end

    delegate :feed_tags, :item_tags, to: RSS::Feed

    class << self

      def parse(url)
        new(url).feed
      end

    end

    # parse feed url and return Feed object
    def parse_feed
      raise InvalidFeedError, "Invalid Feed" unless @source =~ %r{.*?<rss.*?>.*?</rss>}mi

      #channel info
      feed_tag = "rss"
      content = $1 if @source =~ %r{.*?(<#{feed_tag}.*?>.*?</#{feed_tag}>)}mi
      feed_tags.each do |tag|
        content =~ %r{.*?<#{tag}.*?>(.*?)</#{tag}>}mi
        if $1
          var_name = tag_to_var_name(tag)
          tag_content = formatted_content(tag, $1)
          @feed.channel.instance_variable_set(var_name, tag_content)
        end
      end

      #parse to get entries

      @source.scan(%r{<(item|entry).*?>(.*?)</(item|entry)>}mi) do |match|

         item = RSS::Feed::Entry.new
         item_tags.each do |tag|
           value = $1 if match[1] =~ %r{.*?<#{tag}.*?>(.*?)</#{tag}>}mi

           if value

             formatted_value = formatted_content(tag, value)
             item.instance_variable_set(tag_to_var_name(tag), formatted_value)
           end
         end
         if item.valid?
           @feed.items << item
         end
      end
    end

    def tag_to_var_name(tag)
        "@#{tag.to_s.gsub(':','@').intern}"
    end

    # format date and unsescape other content
    def formatted_content(tag, value)
      case tag
        when :published, :updated
          Time.parse(value) rescue unescape(value)
        else
          CGI.unescapeHTML(value)
      end
    end

  end
end
